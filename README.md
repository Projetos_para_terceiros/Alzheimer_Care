# Alzheimer Care

**Progresso:** CANCELADO<br />
**Autor:** Paulo Victor de Oliveira Leal<br />
**Data:** 2015<br />

### Objetivo
Construir um aplicativo para "vigiar" um individuo (com Alzheimer, crianças ou
idosos) em um certo perimetro e que esse individuo sair dessa área algum outro
usuário cadastrado previamente é alertado para busca-lo. 
Desenvolvido durante o Projeto de Aceleração de Startups.

### Observação

IDE:  [Android Studio](https://developer.android.com/studio/)<br />
Linguagem: [JAVA](https://www.java.com/)<br />
Banco de dados: [SQLite](https://www.sqlite.org/)<br />

### Execução

No Android Studio
    
### Contribuição

Esse projeto não está concluído, mas é livre para o uso.

## Licença
<!---

[//]: <> (The Laravel framework is open-sourced software licensed under the [MIT license]https://opensource.org/licenses/MIT)

-->