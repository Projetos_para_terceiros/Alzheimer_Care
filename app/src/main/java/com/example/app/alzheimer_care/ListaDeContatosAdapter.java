package com.example.app.alzheimer_care;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by vinic on 7/20/2015.
 */
public class ListaDeContatosAdapter extends BaseAdapter implements ListAdapter {
    Context mContext;
    ArrayList<Contato> mList = new ArrayList<>();

    public ListaDeContatosAdapter(Context mContext, ArrayList<Contato> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    public void add(Contato nn){
        mList.add(nn);
        notifyDataSetChanged();
        Log.i("umai", "Add");

    }
    public ArrayList<Contato> getmList() {
        return mList;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final int pos = position;

        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.contato_lista, null);
        }

        String nomeContato = mList.get(position).getNome();
        String numeroContato = mList.get(position).getTel();

        TextView nome = (TextView) view.findViewById(R.id.textListaContatosNome);
        nome.setText(nomeContato);

        TextView tel = (TextView) view.findViewById(R.id.textListaContatosNumero);
        tel.setText(""+numeroContato);

        ImageButton delete = (ImageButton) view.findViewById(R.id.buttonContatosDelete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mList.remove(pos);
                notifyDataSetChanged();
            }
        });

        return view;
    }
}
