package com.example.app.alzheimer_care;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

/**
 * Created by Vinicius on 31/05/2015.
 * Inicia o Serviço no boot
 */
public class OnBoot extends BroadcastReceiver
{
    //flag preferencia
    public static final String PREFS_NAME = "Preferences";
    SharedPreferences settings;


    @Override
    public void onReceive(Context context, Intent intent) {
        settings= context.getSharedPreferences(PREFS_NAME, context.MODE_PRIVATE);

        if (settings.getBoolean("checkBoot", false)) {
            Intent serviceIntent = new Intent(context, MyService.class);
            context.startService(serviceIntent);
        }


    }

}