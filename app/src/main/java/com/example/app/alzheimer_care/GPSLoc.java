package com.example.app.alzheimer_care;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;

/**
 * Classe que recupera inlocalização do gps
 * @author Vinicis e Paulo
 */
public final class GPSLoc implements LocationListener {

    private final Context mContext;

    // flag para status GPS
    public boolean isGPSEnabled = false;

    // flag para status  network
    boolean isNetworkEnabled = false;

    // flag para status  GPS
    boolean canGetLocation = false;

    Location location; // localização
    double latitude; // latitude
    double longitude; // longitude

    // Minima distancia para mudar as atualizações em metros
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1; // 10 metros

    // tempo minimo entre atulaizações em milisegundos
    private static final long MIN_TIME_BW_UPDATES = 1; // 1 minuto

    // Declarando a Location Manager
    protected LocationManager locationManager;

    public GPSLoc(Context context) {
        this.mContext = context;
        getLocation();
    }

    /**
     * Funcao que pega a localizacao do usuario
     * @return location - localização obtida
     */
    public Location getLocation() {
        try {

            locationManager = (LocationManager) mContext
                    .getSystemService(Context.LOCATION_SERVICE);

            // obtendo status GPS
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            Log.v("isGPSEnabled", "=" + isGPSEnabled);

            // obtendo status network
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            Log.v("isNetworkEnabled", "=" + isNetworkEnabled);

            if (isGPSEnabled == false && isNetworkEnabled == false) {
                // nenhum provedor de rese ativo
            } else {
                this.canGetLocation = true;
                if (isNetworkEnabled) {
                    location=null;

                    //OBSERVAR O LOOPER
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES,this, Looper.getMainLooper());

                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
                // se GPS ativo, obter lat/long do servido de GPS
                if (isGPSEnabled) {
                    location=null;
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this, Looper.getMainLooper());
                        Log.d("GPS Ativo", "GPS Ativo");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    /**
     * Para o GPS listener Calling, esta função para o uso do gps
     *
     */
    public void stopUsingGPS() {
        if (locationManager != null) {
            locationManager.removeUpdates(GPSLoc.this);
        }
    }

    /**
     * Função para obter latitude
     * @return latitude
     */
    public double getLatitude() {
        if (location != null) {
            latitude = location.getLatitude();
        }

        // return latitude
        return latitude;
    }

    /**
     * Função para obter longitude
     * @return longitude
     */
    public double getLongitude() {
        if (location != null) {
            longitude = location.getLongitude();
        }

        // return longitude
        return longitude;
    }

    /**
     * Checa se  GPS/wifi esta ativo
     * @return boolean
     */
    public boolean canGetLocation() {
        return this.canGetLocation;
    }


    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

}