package com.example.app.alzheimer_care;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.widget.EditText;

/**
 *Classe para criar perfil do paciente
 * AINDA EM CONSTRUÇÃO
 */
public class ActivityPerfil extends ActionBarActivity {
    EditText enome;
    EditText eidade;
    String nome = "";
    String idade = "";

    private AlertDialog alerta;

    //flag preferencia
    public static final String PREFS_NAME = "Preferences";
    SharedPreferences settings = null;

    /**
     * Inicia a activity e recebe dados da interface
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

        //para serviço
        Intent intent = new Intent(getApplicationContext(), MyService.class);
        stopService(intent);

        //inicializa itens da interface
        enome = (EditText) findViewById(R.id.editTextNome);
        eidade = (EditText) findViewById(R.id.editTextIdade);

        //recebe dados existents
       Intent dados = new Intent();
        try {
            dados = getIntent();
        }catch (Exception e){

        }

        if (dados.hasExtra("nome") && dados.hasExtra("idade")) {
            enome.setText(dados.getStringExtra("nome"));
            eidade.setText(dados.getStringExtra("idade"));

        }
    }

    /**
     * Retorna resultados do contato obtido pelo onActivityResult e finaliza a activity
     */
    @Override
    public void finish() {

        if(enome.getText()!=null || eidade.getText()!=null || !enome.getText().toString().equals("Name") || !eidade.getText().toString().equals("Idade") ) {
            settings.edit().putBoolean("perfilOk",true).commit();

            nome = enome.getText().toString();
            idade = eidade.getText().toString();
            System.out.println(nome+""+idade);

            Intent result = new Intent();
            result.putExtra("nome", nome);
            result.putExtra("idade", idade);
            setResult(RESULT_OK, result);
            super.finish();

        }
        else{
            //Cria o gerador do AlertDialog
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            //define o titulo
            builder.setTitle("Dados Invalidos");
            //define a mensagem
            builder.setMessage("Verifique os dados informados");
            //define um botão como positivo
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                }
            });

            //cria o AlertDialog
            alerta = builder.create();
            //Exibe
            alerta.show();
        }


    }


}
