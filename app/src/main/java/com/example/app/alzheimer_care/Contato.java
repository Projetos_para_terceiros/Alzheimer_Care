package com.example.app.alzheimer_care;

/**
 * Created by vinic on 7/20/2015.
 */
public class Contato {
    String nome;
    String tel;

    public Contato(String nome, String tel) {
        this.nome = nome;
        this.tel = tel;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Override
    public String toString() {
        return nome + ':' +
                tel;
    }
}
